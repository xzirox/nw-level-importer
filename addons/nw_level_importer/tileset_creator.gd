func create_from_texture(tileset, save_tileset: bool, save_path_tileset: String):
	print("Creating tileset from texture..")
	
	# Return the created tileset
	return create_tileset(tileset, save_tileset, save_path_tileset)

func create_tileset(tileset_texture: Texture2D, save_tileset: bool, save_path: String):
	# Create a TileSet object
	var tileset: TileSet = TileSet.new()
	tileset.set_tile_shape(0)
	
	# Create an Atlas object
	var atlas: TileSetAtlasSource = TileSetAtlasSource.new()
	atlas.set_texture(tileset_texture)
	
	# Create each tile in the Atlas based on the texture dimensions
	for y in range(round(tileset_texture.get_height()/16)):
		for x in range(round(tileset_texture.get_width()/16)):
			atlas.create_tile(Vector2i(x, y), Vector2i(1, 1))
	
	# Add the Atlas to the TileSet
	tileset.add_source(atlas)
	
	# Save the tileset if we're meant to
	if save_tileset and not save_path == null:
		var dir = DirAccess.open("res://")
		if not dir.dir_exists(save_path.get_base_dir()):
			var error = dir.make_dir_absolute(save_path.get_base_dir())
			if not error == OK:
				save_path = "res://tilesets"
				if not dir.dir_exists("res://tilesets"):
					dir.make_dir_absolute("res://tilesets")
				push_error("Directory could not be found, defaulting to res://tilesets")
		
		if not ResourceSaver.save(tileset, save_path) == OK:
			push_error("TileSet could not be saved!")
	
	return tileset
