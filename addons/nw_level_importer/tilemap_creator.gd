@tool
extends RefCounted

const DEBUG = false

var files = {}

var ignored_paths = [ "res:///.godot" ]
var extentions_to_search_for = [ "png", "gif" ]

func create_from_file(path: String, tileset, save_tileset: bool, save_path_tileset: String):
	print("Starting nw conversion..")
	
	# Scan all subdirectories and cache all the files we need
	get_dir_contents("res://")
	
	# Return the created tilemap
	return create_tilemap(path, tileset)

func create_tilemap(path: String, tileset) -> TileMap:
	var tilemap: TileMap = TileMap.new()
	tilemap.set_tileset(tileset)
	
	var nw_data: Array = load_nw_to_array(path)
	if nw_data[0] != "GLEVNW01":
		push_error("Trying to import a .nw file that doesn't seem to be properly formated")
		return
	nw_data.pop_front()
	
	var GIFS: Array = []
	var MISSING_SPRITES: Array = []
	var data: Dictionary = {}
	var data2: Dictionary = {}
	for row in nw_data:
		var parts = row.split(" ")
		
		if row.begins_with("BOARD "):
			data = {
				"x": int(parts[1]),
				"y": int(parts[2]),
				"width": int(parts[3]),
				"layer": int(parts[4]),
				"tiledata": parts[5]
			}
			
			for x in data.width:
				var atlas: Vector2i = get_atlas_coord_from_string(data.tiledata.substr(x*2, 2))
				tilemap.set_cell(0, Vector2i(x, data.y), 0, atlas)
		elif row.begins_with("NPC "):
			data = {
				"image": parts[1],
				"x": int(parts[2]),
				"y": int(parts[3])
			}
			if data.image.get_extension() == "gif":
				if not GIFS.has(data.image):
					GIFS.push_back(data.image)
				data.original = data.image
				data.image = "icon_32x32.png"
		elif not data.is_empty() and row.strip_edges().begins_with("setimgpart"):
			var parts2 = row.split(";")[0].split(",")
			data2 = {
				"x": parts2[1],
				"y": parts2[2],
				"width": parts2[3],
				"height": parts2[4]
			}
			var expression = Expression.new()
			for key in data2:
				expression.parse(data2[key])
				data2[key] = expression.execute()
		elif row == "NPCEND" and not data.is_empty():
			if data.image.get_extension() != "":
				if data.image in files.keys():
					var node: Sprite2D = Sprite2D.new()
					node.texture = load(files[data.image])
					if not data2.is_empty():
						node.region_enabled = true
						node.region_rect = Rect2i(Vector2i(data2.x, data2.y), Vector2i(data2.width, data2.height))
						data2.clear()
					node.set_position(Vector2i(data.x+1, data.y+1)*16)
					node.set_name("{0} {1}".format([data.original if "original" in data.keys() else data.image, str(node.get_instance_id())]))
					node.set_position(Vector2i(data.x+1, data.y+1)*16)
					tilemap.add_child(node)
					node.set_owner(tilemap)
				elif data.image.get_extension() in extentions_to_search_for:
					if not MISSING_SPRITES.has(data.image):
						MISSING_SPRITES.push_back(data.image)
			data.clear()
	if not MISSING_SPRITES.is_empty():
		push_warning("Missing sprites:")
		for sprite in MISSING_SPRITES:
			push_warning("- {0}".format([sprite]))
		push_warning("To fix missing sprites from the level, include them somewhere in your Godot project and reimport the level file.")
	if not GIFS.is_empty():
		push_warning("Missing gifs:")
		for sprite in GIFS:
			push_warning("- {0}".format([sprite]))
		push_warning("gif is an outdated format that is not supported, please convert to png if you want to use.")
	
	var sorted_nodes := tilemap.get_children()
	sorted_nodes.sort_custom(
		# For descending order use > 0
		func(a: Node, b: Node): return a.name.naturalnocasecmp_to(b.name) < 0
	)
	for node in tilemap.get_children():
		tilemap.remove_child(node)
	for node in sorted_nodes:
		tilemap.add_child(node)
	
	# Pack & save the scene for debug purposes
	if DEBUG:
		var packed_scene = PackedScene.new()
		packed_scene.pack(tilemap)
		ResourceSaver.save(packed_scene, "res://debug.tscn")
	
	return tilemap

func load_nw_to_array(path: String):
	var file: FileAccess = FileAccess.open(path, FileAccess.READ)
	var content: Array = file.get_as_text().strip_edges().split("\n")
	return content

func get_atlas_coord_from_string(string: String):
	var BASE64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
	var lhs = BASE64.find(string[0]) * 64
	var rhs = BASE64.find(string[1])
	var di = lhs + rhs
	var tx = di % 16
	var ty = di / 16
	var bx = ty / 32 * 16 + tx
	var by = ty % 32
	return Vector2i(bx, by)

func get_dir_contents(rootPath: String) -> void:
	var dir = DirAccess.open(rootPath)

	if dir.get_open_error() == OK:
		dir.list_dir_begin()
		_add_dir_contents(dir)
	else:
		push_error("An error occurred when trying to access the path.")

func _add_dir_contents(dir: DirAccess):
	var file_name = dir.get_next()

	while (file_name != ""):
		var path = dir.get_current_dir() + "/" + file_name
		
		if dir.current_is_dir():
			if not path in ignored_paths:
				if path.begins_with("res://addons/nw_level_importer") or not path.begins_with("res://addons"):
					#print("Found directory: %s" % path)
					var subDir = DirAccess.open(path)
					subDir.list_dir_begin()
					_add_dir_contents(subDir)
		else:
			if not path.get_base_dir() == "res://addons":
				if path.get_extension() in extentions_to_search_for:
					#print("Found file: %s" % path)
					files[path.get_file()] = path

		file_name = dir.get_next()

	dir.list_dir_end()
