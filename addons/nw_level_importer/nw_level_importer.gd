@tool
extends EditorPlugin

var tilemap_import = null

func get_name():
	return ".nw Level Importer"

func _enter_tree():
	tilemap_import = preload("res://addons/nw_level_importer/tilemap_import.gd").new()
	add_import_plugin(tilemap_import)


func _exit_tree():
	remove_import_plugin(tilemap_import)
	tilemap_import = null
