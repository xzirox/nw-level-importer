@tool
extends EditorImportPlugin

enum Presets { DEFAULT, GODOT_TILESET }

func _get_importer_name():
	return "nw_level_importer"
	
func _get_visible_name():
	return ".nw Level Importer"

func _get_recognized_extensions():
	return ["nw"]

func _get_save_extension():
	return "scn"

func _get_resource_type():
	return "PackedScene"

func _get_priority():
	return 1

func _get_preset_count():
	return Presets.size()

func _get_preset_name(preset_index):
	match preset_index:
		Presets.DEFAULT: return "Default"
		Presets.GODOT_TILESET: return "Godot Tileset"

func _get_import_options(path, preset_index):
	match preset_index:
		Presets.DEFAULT, Presets.GODOT_TILESET:
			# Shared options
			var options = [
				{
					name = "use_godot_tileset",
					default_value = (preset_index == Presets.GODOT_TILESET),
				}
			]
			
			# .png Texture2D options
			options.append_array([
				{
					"name": "png",
					"default_value": null,
					"property_hint": PROPERTY_HINT_NONE,
					"hint_string": "png/"
				},
				{
					"name": "png/image",
					"default_value": load("res://addons/nw_level_importer/assets/pics1.png"),
					"property_hint": PROPERTY_HINT_RESOURCE_TYPE,
					"hint_string": "CompressedTexture2D"
				},
				{
					"name": "png/save",
					"default_value": true,
					"property_hint": PROPERTY_HINT_NONE,
				},
				{
					"name": "png/directory",
					"default_value": "res://tilesets/",
					"property_hint": PROPERTY_HINT_DIR,
				},
				{
					"name": "png/file_name",
					"default_value": "tileset.tres",
					"property_hint": PROPERTY_HINT_NONE
				}
			])
			
			# Godot Tileset options
			options.append_array([
				{
					"name": "tileset",
					"default_value": null,
					"property_hint": PROPERTY_HINT_NONE,
					"hint_string": "godot_tileset/",
					"usage": PROPERTY_USAGE_CATEGORY
				},
				{
					"name": "tileset/scene",
					"default_value": load("res://addons/nw_level_importer/assets/pics1.tres"),
					"property_hint": PROPERTY_HINT_RESOURCE_TYPE,
					"hint_string": "TileSet"
				}
			])

			return options
		_:
			return []

func _get_import_order():
	return 99

func _get_option_visibility(path, option_name, options):
	# Godot tileset options visibility
	if options.get("use_godot_tileset", false):
		if option_name.begins_with("tileset/"):
			return true
	else:
		if option_name.begins_with("png/"):
			if not options["png/save"]:
				if option_name == "png/directory" or option_name == "png/directory":
					return false
				else:
					return true
			else:
				return true
	
	return false

func _import(source_file, save_path, options, platform_variants, gen_files):
	if !FileAccess.file_exists(source_file):
		return "No File found"
	
	var tilemap_creator = load("res://addons/nw_level_importer/tilemap_creator.gd").new()
	var tileset_creator = load("res://addons/nw_level_importer/tileset_creator.gd").new()
	
	var tileset = (options["tileset/tileset"] if options["use_godot_tileset"] else options["png/image"])
	var save_tileset = (options["png/save"] if not options["use_godot_tileset"] else false)
	var save_path_tileset = ("{0}{1}".format([options["png/directory"],options["png/file_name"]]) if save_tileset else null)
	
	if typeof(tileset) != TYPE_OBJECT:
		push_warning("Provided tileset is not an object, aborting")
		return
	
	tileset = tileset_creator.create_from_texture(tileset, save_tileset, save_path_tileset)
	var tilemap = tilemap_creator.create_from_file(source_file, tileset, save_tileset, save_path_tileset)
	
	var packed_scene = PackedScene.new()
	packed_scene.pack(tilemap)
	return ResourceSaver.save(packed_scene, "%s.%s" % [save_path, _get_save_extension()])
