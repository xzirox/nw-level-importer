# nw Level Importer



## About
This Godot addon allows you to import .nw level files used by the Graal Online game engine and use them in Godot as a TileMap object.

Current features include:
- Import a .nw level as a TileMap
- Load NPCs as Sprite2D objects. If the image is invalid or missing from your project, warnings will be shown.
- Select tileset when importing the level using two different presets:
   - Default = creates a TileSet resource from a provided .png image
   - Godot Tilemap = uses a provided TileSet for the level


Planned features:
- Import .gmap as a large TileMap
- Generate one directional "NPCs" from level data
